package utils;

import java.util.Collection;
import java.util.stream.Stream;

public class CustomCollectionUtils {

    public static <O> Stream<O> safeStreamOf(Collection<O> collection) {
        return collection == null ? Stream.empty() : collection.stream();
    }
}
