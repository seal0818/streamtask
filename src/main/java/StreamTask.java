import interfaces.NamedObject;
import objects.Grouper;
import objects.SomeObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class StreamTask {

    private static final Random RANDOMIZER = new Random();

    public static void main(String[] args) {
        List<NamedObject> objects = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            objects.add(new SomeObject(StreamTask.generateName(i)));
        }

        Map<String, List<NamedObject>> result = Grouper.groupByName(objects);
        System.out.println(result.toString());
    }

    private static String generateName(int i) {
        return "Name" + RANDOMIZER.nextInt(i + 1);
    }
}
