package interfaces;

public interface NamedObject {
    String getName();
}
