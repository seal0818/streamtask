package objects;

import interfaces.NamedObject;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static utils.CustomCollectionUtils.safeStreamOf;

public class Grouper {

    public static Map<String, List<NamedObject>> groupByName(List<NamedObject> namedObjects) {
        return safeStreamOf(namedObjects)
                .collect(Collectors.groupingBy(NamedObject::getName));
    }
}
