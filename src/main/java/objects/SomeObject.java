package objects;

import interfaces.NamedObject;

public class SomeObject implements NamedObject {

    private final String name;

    public SomeObject(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
